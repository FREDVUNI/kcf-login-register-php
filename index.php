<?php
session_start();
if(!isset($_SESSION['user_id'])):
  header("Location:./login.php?error=You're not logged in.");
endif;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login/Register Form</title>
  <link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
  />  
  <link
  href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
  rel="stylesheet"
  />
  <link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
  />
</head>
<style>
    body{
        font-family: "Poppins", sans-serif;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">todo app</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#services">Services</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contact">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-success" href="#"><?php echo $_SESSION['name'];?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-success" href="./controllers/logout.php">Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container">
  <div class="row" id="home">
    <div class="col-md-6">
      <div class="hero-section">
        <h1 class="text-left my-5">Welcome to the todo app</h1>
        <p class="text-left">
            A todo list is a tool used to manage tasks or activities that need to be completed within a certain timeframe. It usually consists of a list of items with checkboxes or other markers that allow you to indicate whether a task has been completed or not.
        </p>
        <div class="text-left mt-5">
          <button type="button" class="btn btn-primary btn-lg">Learn More</button>
        </div>
      </div>
    </div>
    <div class="col-md-6 mt-4">
        <img src="https://plus.unsplash.com/premium_photo-1683309563529-05b8d9619e71?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="image" class="img-fluid">
    </div>
  </div>
  <section id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mt-5 mb-5">
        <h2 class="text-center">Our Services</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="mb-4">
        <i class="fas fa-list fa-2x mb-2"></i>
            <h4>Manage tasks</h4>
            <p>A todo list is a tool used to manage tasks or activities that need to be completed.</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-4">
        <i class="fas fa-clock fa-2x mb-2"></i>
            <h4>A certain timeframe</h4>
            <p>A todo list is a tool used to manage tasks or activities that need to be completed.</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-4">
        <i class="fas fa-book fa-2x mb-2"></i>
            <h4>List of items</h4> 
            <p>A todo list is a tool used to manage tasks or activities that need to be completed.</p>
        </div>
      </div>
    </div>
  </div>
</section>
  <section id="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mt-5">
        <h2>About Us</h2>
        <p>A todo list is a tool used to manage tasks or activities that need to be completed within a certain timeframe. It usually consists of a list of items with checkboxes or other markers that allow you to indicate whether a task has been completed or not.</p>
        <p>A todo list is a tool used to manage tasks or activities that need to be completed within a certain timeframe. It usually consists of a list of items with checkboxes or other markers that allow you to indicate whether a task has been completed or not.</p>
        <p>A todo list is a tool used to manage tasks or activities that need to be completed within a certain timeframe. It usually consists of a list of items with checkboxes or other markers that allow you to indicate whether a task has been completed or not.</p>
        <div class="text-left mt-5">
          <button type="button" class="btn btn-primary btn-lg">Learn More</button>
        </div>      
      </div>
    </div>
  </div>
</section>
  <section id="contact" class="mb-5 mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-4 mt-4">
        <div class="card h-100 mb-4 shadow-sm">
          <div class="card-body">
            <i class="fas fa-phone fa-3x mb-2"></i>
            <h4 class="card-title">Phone</h4>
            <p class="card-text">Call us at +1 (123) 456-7890</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mt-4">
        <div class="card h-100 mb-4 shadow-sm">
          <div class="card-body">
            <i class="fas fa-envelope fa-3x mb-2"></i>
            <h4 class="card-title">Email</h4>
            <p class="card-text">Send us an email at info@yourdomain.com</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mt-4">
        <div class="card h-100 mb-4 shadow-sm">
          <div class="card-body">
            <i class="fas fa-map-marker-alt fa-3x mb-2"></i>
            <h4 class="card-title">Address</h4>
            <p class="card-text">1234 Main St, Suite 200, New York, NY 10001</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
