# User Registration Form for a To-Do List Application

This is a simple user registration form for a to-do list application built with PHP and a web server. The form collects the user's name, email address, and password. Upon submission, the user's information is stored in a MySQL database.

## Functionality

When a user submits the form, the application checks whether the email address already exists in the database. If the email address is already in use, the user is notified and prompted to enter a different email address. If the email address is not already in use, the user's information is stored in the database and they are redirected to a login page.

## Technologies Used

- PHP
- MySQL
- Bootstrap
- FontAwesome
- Google Fonts

## How to Use

1. Clone the repository.
2. Set up a web server with PHP and MySQL.
3. Import the `database/tasks_todo.sql` file into your MySQL database.
4. In `database/database.php`, update the database connection details.
5. Navigate to `register.php` and fill out the registration form.
6. If successful, you will be redirected to the login page.

## Contributors

- [VUNI](https://github.com/FREDVUNI)

## License

This project is licensed under the [MIT License](LICENSE).