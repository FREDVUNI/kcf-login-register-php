<?php include_once("./includes/header.php");?>
<body>
  <div class="container d-flex justify-content-center">
    <div class="card col-md-6">
      <div class="card-body">
        <h2 class="card-title text-center mb-4 font-weight-bold">Login</h2>
        <?php 
          if(isset($_GET['error'])) {
            echo '<div class="alert alert-danger">' . $_GET['error'] . '</div>';
          }
        ?>
        <form action="./controllers/login.php" method="post">
        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input type="email" class="form-control" name="email" placeholder="Enter the email address" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Enter the password" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
        </div>
          <div class="d-grid gap-2 mb-5">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
            <span class="cta mb-4 text-muted">
            Don't an account yet? register<a href="./register.php"> Here</a>
          </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
