<?php 
include_once("./includes/header.php");?>
<body>
  <div class="container d-flex justify-content-center">
    <div class="card col-md-6">
      <div class="card-body">
        <h2 class="card-title text-center mb-4 font-weight-bold">Register</h2>
        <?php 
          if(isset($_GET['error'])) {
            echo '<div class="alert alert-danger">' . $_GET['error'] . '</div>';
          }
        ?>
        <form action="./controllers/register.php" method="post">
        <div class="mb-3">
            <label for="name" class="form-label">Full Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter the full name">
          </div>
          <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" name="email" placeholder="Enter the email address">
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Enter the password">
          </div>
          <div class="d-grid gap-2 mb-5">
            <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
            <span class="cta mb-4 text-muted">
            Already have an account ? login<a href="./login.php"> Here</a>
          </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
