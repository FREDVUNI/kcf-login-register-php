<?php
require_once("../database/database.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $errors = array();

  if (!isset($_POST['email']) || empty(trim($_POST['email']))) {
    $errors[] = 'Email is required';
  } else {
    $email = trim($_POST['email']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors[] = 'Invalid email format';
    }
  }

  if (!isset($_POST['password']) || empty(trim($_POST['password']))) {
    $errors[] = 'Password is required';
  } else {
    $password = trim($_POST['password']);
    if (strlen($password) < 8) {
      $errors[] = 'Password must be at least 8 characters long';
    }
  }

  if (empty($errors)) {
    $stmt = mysqli_prepare($conn, "SELECT * FROM users WHERE email = ?");
    mysqli_stmt_bind_param($stmt, "s", $email);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    if (mysqli_num_rows($result) == 1) {
      $row = mysqli_fetch_assoc($result);
      if (password_verify($password, $row['password'])) {
        session_start();
        $_SESSION['user_id'] = $row['id'];
        $_SESSION['name'] = $row['name'];
        $_SESSION['email'] = $row['email'];

        header('Location: ../index.php');
        exit;
      } else {
        $errors[] = 'Wrong email or password';
      }
    } else {
      $errors[] = 'Wrong email or password';
    }
  }

  mysqli_close($conn);

  if (!empty($errors)) {
    $error_string = implode('&', $errors);
    header("Location: ../login.php?error=$error_string");
    exit;
  }
}
