<?php
require_once("../database/database.php");
if (isset($_POST["submit"]) && !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password'])):
  $name = $_POST['name'];
  $email = $_POST['email'];
  $password = $_POST['password'];

  $sql = "SELECT * FROM users WHERE email = ?";
  $stmt = $conn->prepare($sql);
  $stmt->bind_param("s", $email);
  $stmt->execute();
  $result = $stmt->get_result();

  $hash = password_hash($password,PASSWORD_DEFAULT);

  if ($result->num_rows === 0):
      $stmt = $conn->prepare("INSERT INTO users (name,email,password) VALUES (?, ?, ?)");
      $stmt->bind_param("sss", $name,$email,$hash);
      $stmt->execute();
      $stmt->close();
      header("Location: ../login.php");
  elseif ($result->num_rows === 1):
      header("Location: ../register.php?error=User already exists.");
  else:
      header("Location: ../register.php?error=Database error.");
  endif;

  $conn->close();

else:
  header("Location: ../register.php?error=All fields are required.");
endif;

?>